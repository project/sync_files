# Sync Files

This tiny module can be used to synchronize files on the dev/stage/cloned site with one click. Just imagine the scenario:

- You create a copy of the production site using git clone for development, backup, etc
- You are importing a database dump
- You want to view images, videos, audio and other media files or document files, but this is a very large archive, or you do not have direct access to the console and you need to wait until permissions are granted
- You want to synchronize files during development - you get a database dump, but wait files as usual
- You have received an incomplete or corrupted archive with files from the administrators of the production site

This is a module designed to eliminate a bottleneck. Just enter the address of the production server and click the sync button!

This module takes file URLs from your target DB, change the domain to domain from settings and use that URL as a file source, so source files for sync should be available to your site via HTTP(S) protocol.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sync_files).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sync_files).


## Requirements

This module requires no modules outside of Drupal core, but need CURL PHP extension installed.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configuration path `/admin/config/system/sync_files`


## Maintainers

- [Andrew Answer](https://www.drupal.org/u/andrew-answer) - 
  [site link](https://it.answe.ru) | [mail](mail@answe.ru) | [telegram](https://t.me/andrew_answer)
