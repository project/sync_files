<?php

namespace Drupal\sync_files\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Link;
use Drupal\Core\File\FileExists;
use Drupal\sync_files\SyncFilesBatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a SyncFiles form.
 */
class SyncFilesForm extends FormBase {

  /**
   * Manages entity type plugin definitions.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Manages entity type plugin definitions.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Constructs for SyncFilesForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Service entity_type.manager.
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   Service entity_type.manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager, FileSystem $fileSystem) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sync_files';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $array_sync_files = $this->syncFiles();
    $count_sync_files = count($array_sync_files);
    if ($count_sync_files == 0) {
      $form['#access'] = FALSE;
    }
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync @count files', ['@count' => $count_sync_files]),
      '#attributes' => ['class' => ['button--primary']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('sync_files.settings');
    $url_server = $config->get('address_server');
    $status_server = $this->serverAvailible($url_server);
    $text_error = $this->t('This server is unavailable, please @href.',
      ['@href' => Link::createFromRoute('enter another address', 'sync_files.settings_form')->toString()]);
    if ($status_server == FALSE) {
      $form_state->setErrorByName('submit', $text_error);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $array_sync_files = $this->syncFiles();
    $config = $this->config('sync_files.settings');
    $url_server = $config->get('address_server');
    $import = new SyncFilesBatch($array_sync_files, $url_server);
    $import->setBatch();
  }

  /**
   * {@inheritdoc}
   */
  public function syncFiles() {
    $query = $this->entityTypeManager->getStorage('file')->getQuery();
    // See https://www.drupal.org/node/3201242.
    $files = $query
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();
    $sync_files = [];
    foreach ($files as $value) {
      $file = $this->entityTypeManager->getStorage('file')->load($value);
      $file_uri = $file->uri->value;
      // See https://www.drupal.org/node/3426517.
      [$version, $version_minor] = explode('.', \Drupal::VERSION);
      if (intval($version) == 10 && intval($version_minor) >= 3) {
        $state = \Drupal\Core\File\FileExists::Error;
      }
      else {
        $state = \Drupal\Core\File\FileSystemInterface::EXISTS_ERROR;
      }
      $file_exists_error = \Drupal::service('file_system')->getDestinationFilename($file_uri, $state);
      if ($file_exists_error == FALSE) {
        continue;
      }
      else {
        $sync_files[] = $value;
      }
    }
    return $sync_files;
  }

  /**
   * {@inheritdoc}
   */
  public function serverAvailible($url_server) {
    // Filter PHP correct URL server.
    if (!filter_var($url_server, FILTER_VALIDATE_URL)) {
      return FALSE;
    }
    // Initialization СURL.
    $curlInit = curl_init($url_server);
    // Request Parameters.
    curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curlInit, CURLOPT_HEADER, TRUE);
    curl_setopt($curlInit, CURLOPT_NOBODY, TRUE);
    curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, TRUE);
    // Get response.
    $response = curl_exec($curlInit);
    // Close CURL.
    curl_close($curlInit);
    return $response ? TRUE : FALSE;
  }

}
