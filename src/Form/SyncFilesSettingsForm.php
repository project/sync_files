<?php

namespace Drupal\sync_files\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure SyncFiles settings for this site.
 */
class SyncFilesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sync_files_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sync_files.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['address_server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address server'),
      '#default_value' => $this->config('sync_files.settings')->get('address_server'),
      '#description' => $this->t('Enter the server address in the http:// or https://'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $url_server = $form_state->getValue('address_server');
    if ($url_server == '') {
      $form_state->setErrorByName('address_server', $this->t('The value is not correct.'));
    }
    if (!filter_var($url_server, FILTER_VALIDATE_URL)) {
      $form_state->setErrorByName('address_server', $this->t('The address is not correct.'));
    }
    $status_server = $this->serverAvailible($url_server);
    if ($status_server == FALSE) {
      $form_state->setErrorByName('address_server', $this->t('This server is unavailable.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sync_files.settings')
      ->set('address_server', $form_state->getValue('address_server'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function serverAvailible($url_server) {
    // Filter PHP correct URL server.
    if (!filter_var($url_server, FILTER_VALIDATE_URL)) {
      return FALSE;
    }
    // Initialization СURL.
    $curlInit = curl_init($url_server);
    // Request Parameters.
    curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curlInit, CURLOPT_HEADER, TRUE);
    curl_setopt($curlInit, CURLOPT_NOBODY, TRUE);
    curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, TRUE);
    // Get response.
    $response = curl_exec($curlInit);
    // Close CURL.
    curl_close($curlInit);
    return $response ? TRUE : FALSE;
  }

}
