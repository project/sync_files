<?php

namespace Drupal\sync_files\Plugin\views\area;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sync_files\Form\SyncFilesForm;

/**
 * SyncFiles custom form area handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("sync_files_area")
 */
class SyncFilesArea extends AreaPluginBase {
  /**
   * Description message.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('form_builder'));
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    return $this->formBuilder->getForm(SyncFilesForm::class);
  }

}
