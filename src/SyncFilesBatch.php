<?php

namespace Drupal\sync_files;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\Core\Render\Markup;

/**
 * Syncs files in batches.
 *
 * @package Drupal\sync_files
 */
class SyncFilesBatch {

  use StringTranslationTrait;


  /**
   * List of files that needs to be synced.
   *
   * @var array
   */
  protected array $array_sync_files;

  /**
   * The address of the server to connect.
   *
   * @var string
   */
  protected string $url_server;

  /**
   * Hold's details on the current batch job.
   *
   * @var array
   */
  protected array $batch;

  /**
   * Constructs a SyncFilesBatch object.
   *
   * @param mixed $array_sync_files
   *   The param from form.
   * @param mixed $url_server
   *   The param from form.
   */
  public function __construct($array_sync_files, $url_server) {
    $this->array_sync_files = $array_sync_files;
    $this->url_server = $url_server;
    $this->batch = [
      'title' => 'Synchronization files',
      'finished' => [$this, 'finished'],
    ];
    $this->splitPartsArray();
  }

  /**
   * {@inheritdoc}
   */
  public function splitPartsArray() {
    foreach (array_chunk($this->array_sync_files, 10) as $items_sync_files) {
      $this->setOperation($items_sync_files);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setOperation($items) {
    foreach ($items as $item) {
      $this->batch['operations'][] = [
        [$this, 'processItem'],
        [$item],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item, &$context) {
    $file = File::load($item);
    // Relative URL file.
    $file_url = $file->createFileUrl();
    // Absolute URL file to server source.
    $file_url_server = $this->url_server . $file_url;
    $file_system = \Drupal::service('file_system');
    // Get file from server source.
    $content = file_get_contents($file_url_server);
    if ($content != FALSE) {
      // Get root directory in this site.
      $dir_root = $_SERVER['DOCUMENT_ROOT'];
      // Get recurse directory where to file.
      $dir_file = $file_system->dirname($file_url);
      // Check directory, if not, then create.
      if (!is_dir($dir_root . $dir_file)) {
        mkdir($dir_root . $dir_file, 0777, TRUE);
      }
      // Save file to directory.
      file_put_contents($dir_root . rawurldecode($file_url), $content);
      // Write succeed result.
      if (empty($context['results']['synchronization_files']['succeed'])) {
        $context['results']['synchronization_files']['succeed'] = 1;
      }
      else {
        $context['results']['synchronization_files']['succeed']++;
      }
    }
    else {
      // Write error result.
      if (empty($context['results']['synchronization_files']['error']['count'])) {
        $context['results']['synchronization_files']['error']['count'] = 1;
      }
      else {
        $context['results']['synchronization_files']['error']['count']++;
      }
      $context['results']['synchronization_files']['error']['name'][] = $file->getFilename();
    }
    // Write all result.
    if (empty($context['results']['synchronization_files']['all'])) {
      $context['results']['synchronization_files']['all'] = 1;
    }
    else {
      $context['results']['synchronization_files']['all']++;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setBatch() {
    batch_set($this->batch);
  }

  /**
   * {@inheritdoc}
   */
  public function processBatch() {
    batch_process();
  }

  /**
   * {@inheritdoc}
   */
  public function finished($success, $results, $operations, $elapsed) {
    if ($success) {
      if (!empty($results['synchronization_files']['all']) && empty($results['synchronization_files']['error'])) {
        $message_sync_all = $this->t('Synchronized @succeed_count files from @all_count files',
        [
          '@succeed_count' => $results['synchronization_files']['succeed'],
          '@all_count' => $results['synchronization_files']['all'],
        ]);
        \Drupal::messenger()->addStatus($message_sync_all);
      }
      elseif (!empty($results['synchronization_files']['all']) && !empty($results['synchronization_files']['error'])) {
        // Add message synchronized succeed.
        if (!empty($results['synchronization_files']['succeed'])) {
          $sync_count = $results['synchronization_files']['succeed'];
        }
        else {
          $sync_count = 0;
        }
        $message_sync_succeed = $this->t(
          'Synchronized @sync_count from @all_count files.',
        [
          '@sync_count' => $sync_count,
          '@all_count' => $results['synchronization_files']['all'],
        ]);
        \Drupal::messenger()->addStatus($message_sync_succeed);

        // Add message synchronized error.
        $files = implode('<br>', $results['synchronization_files']['error']['name']);
        $rendered_files = Markup::create($files);
        $text_sync_error = $this->t(
          '<p>@error_count files are not synchronized.</p>
          <p>These source files are absent:</p>
          <p>@files</p>',
          [
            '@error_count' => $results['synchronization_files']['error']['count'],
            '@files' => $rendered_files,
          ]);
        $message_sync_error = $text_sync_error;
        \Drupal::messenger()->addWarning($message_sync_error);
      }
    }
    else {
      $message_error = $this->t('Finished with an error.');
      \Drupal::messenger()->addError($message_error);
    }
  }

}
