<?php

/**
 * @file
 * Provide views data that isn't tied to any other module.
 */

/**
 * Implements hook_views_data().
 */
function sync_files_views_data() {
  $data['views']['sync_files_area'] = [
    'title' => 'Synchronization files custom form',
    'help' => 'Provides a custom form for synchronization files.',
    'area' => [
      'id' => 'sync_files_area',
    ],
  ];
  return $data;
}
